//
//  User.swift
//  Event
//
//  Created by Cheick Mahady SISSOKO on 03/01/2016.
//  Copyright © 2016 Telecom ParisTech. All rights reserved.
//

import Foundation

struct User {
    let id: Int!
    let first_name: String!
    let last_name: String!
    let login: String!
    let password: String?
    
    let created: NSDate!
    let modified: NSDate!
    
    init?(data: NSDictionary?) {
        id = (data?.valueForKey(UserKey.ID) as? String)?.intValue
        login = data?.valueForKey(UserKey.Login) as? String
        first_name = data?.valueForKey(UserKey.FistName) as? String
        last_name = data?.valueForKey(UserKey.LastName) as? String
        
        password = data?.valueForKey(UserKey.Password) as? String
        
        created = (data?.valueForKey(UserKey.Created) as? String)?.asDate
        modified = (data?.valueForKey(UserKey.Modified) as? String)?.asDate
        if login == nil {
            return nil
        }
    }
    
    struct UserKey {
        static let ID = "id"
        static let FistName = "first_name"
        static let LastName = "last_name"
        static let Login = "login"
        static let Password = "password"
        
        static let Created = "create_date"
        static let Modified = "modify_date"
        
    }
}

private extension String {
    var asDate: NSDate? {
        get {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            return dateFormatter.dateFromString(self)
        }
    }
    
    var intValue: Int {
        return (self as NSString).integerValue
    }
}
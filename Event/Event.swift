//
//  Event.swift
//  Event
//
//  Created by Cheick Mahady SISSOKO on 03/01/2016.
//  Copyright © 2016 Telecom ParisTech. All rights reserved.
//

import Foundation

struct Event
{
    let id: Int!
    let title: String!
    let content: String!
    let date: NSDate!
    let address: String!
    let cost: Float!
    let iban: String!
    let author: User!
    
    let created: NSDate!
    let modified: NSDate!
    
    init?(data: NSDictionary?) {
        author = User(data: data?.valueForKeyPath(EventKey.Author) as? NSDictionary)
        id = (data?.valueForKey(EventKey.ID) as? String)?.intValue
        title = data?.valueForKey(EventKey.Title) as? String
        content = data?.valueForKey(EventKey.Content) as? String
        date = (data?.valueForKey(EventKey.Date) as? String)?.asDate
        address = data?.valueForKey(EventKey.Address) as? String
        cost = (data?.valueForKey(EventKey.Cost) as? String)?.floatValue
        iban = data?.valueForKey(EventKey.Iban) as? String
        
        created = (data?.valueForKey(EventKey.Created) as? String)?.asDate
        modified = (data?.valueForKey(EventKey.Modified) as? String)?.asDate
        if title == nil || content == nil || date == nil {
            return nil
        }
    }
    
    
    struct EventKey {
        static let ID = "id"
        static let Title = "title"
        static let Content = "content"
        static let Date = "date"
        static let Address = "address"
        static let Cost = "cost"
        static let Iban = "iban"
        static let Author = "author"
        
        static let Created = "create_date"
        static let Modified = "modify_date"
    }
}

private extension String {
    var asDate: NSDate? {
        get {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            return dateFormatter.dateFromString(self)
        }
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    var intValue: Int {
        return (self as NSString).integerValue
    }
}